package com.example.task3mariabravo;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

public class MainActivity extends AppCompatActivity {

    private FirebaseAnalytics myFireBaseAnalytics;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;

    // Remote Config keys
    private static final String SHOP_CORONAVIRUS_BUTTON = "products_coronavirus";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myFireBaseAnalytics = FirebaseAnalytics.getInstance(this);


        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);

        mFirebaseRemoteConfig.fetch(3600)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(MainActivity.this, "Fetch Succeeded",
                                    Toast.LENGTH_SHORT).show();

                            // After config data is successfully fetched, it must be activated before newly fetched
                            // values are returned.
                            mFirebaseRemoteConfig.activateFetched();

                            String shopButton = mFirebaseRemoteConfig.getString(SHOP_CORONAVIRUS_BUTTON);
                            Button bShop = findViewById(R.id.btnShop);
                            bShop.setText(shopButton);

                        } else {
                            Toast.makeText(MainActivity.this, "Fetch Failed",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });


        myFireBaseAnalytics.setUserProperty("user_type","standars");



        Button bLogin = findViewById(R.id.btnLogin);
        bLogin.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v){
                Context context = getApplicationContext();
                CharSequence text = "Click on login";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

                myFireBaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, null);
            }
        });

        Button bShop = findViewById(R.id.btnShop);
        bShop.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v){
                Context context = getApplicationContext();
                CharSequence text = "Click on shop our products";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

                myFireBaseAnalytics.logEvent("shop",null);


            }
        });

        Button bSubscribe = findViewById(R.id.btnSubscribe);
        bSubscribe.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v){
                Context context = getApplicationContext();
                CharSequence text = "Click on subscribe";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

                myFireBaseAnalytics.logEvent("subscribe",null);
            }
        });

        Button bReturn = findViewById(R.id.btnReturnProduct);
        bReturn.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v){
                Context context = getApplicationContext();
                CharSequence text = "Click on return a product";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

                myFireBaseAnalytics.logEvent("return",null);
            }
        });

        Button bShare = findViewById(R.id.btnShare);
        bShare.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v){
                Context context = getApplicationContext();
                CharSequence text = "Click on share";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

                myFireBaseAnalytics.logEvent(FirebaseAnalytics.Event.SHARE,null);
            }
        });

    }
}
